#include <iostream>
#include <numeric>
#include <ctime>

int main()
{
	const auto N = 10;

	std::time_t currentTime = std::time(nullptr);
	tm currentTimeComponents;
	localtime_s(&currentTimeComponents, &currentTime);

	//const auto Date = 12;
	const auto Date = currentTimeComponents.tm_mday;

	int Array[N][N];

	for (auto i = 0; i < N; ++i)
		for (auto j = 0; j < N; ++j)
			Array[i][j] = i + j;

	for (const auto& l : Array)
	{
		for (const auto& c : l)
			std::cout << c << '\t';

		std::cout << std::endl;
	}

	const auto Line = Date % N;
	const auto Sum = std::accumulate(std::begin(Array[Line]), std::end(Array[Line]), 0);

	std::cout << "Date = " << Date << ", Line = " << Line << ", Sum = " << Sum << std::endl;
}